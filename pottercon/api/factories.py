import datetime
import factory
import hashlib
import uuid

from . import models


class EventTypeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.EventType
        django_get_or_create = ('name',)

    name = 'Event Type Test'
    icon = 'eventype.jpg'


class EventFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Event
        django_get_or_create = ('name',)

    date = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    name = 'Sorteo Varita'
    location = 'whatever'
    type = factory.SubFactory(EventTypeFactory)
    organizer = 'Jhon Doe'
    icon = 'sorteo_varita.jpg'


class HouseFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.House
        django_get_or_create = ('name',)

    name = 'Griffindor'
    image = 'griffindor.jpg'


class CollectibleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Collectible
        django_get_or_create = ('id',)

    id = factory.Sequence(lambda n: '{}{}'.format(uuid.uuid4, n))
    name = 'Collectible test'
    description = 'Collectible Desc'
    icon = 'collectible.jpg'
    points = 10
    type = models.CollectibleType.objects.get(pk=1)
    representation = 'representation.jpg'


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.ApiUser
        django_get_or_create = ('name',)

    name = 'Harry'
    device_id = hashlib.sha256(b'123').hexdigest()
    image = 'harry.jpg'
    house = models.House.objects.get(name='Gryffindor')
