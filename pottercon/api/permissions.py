from rest_framework import permissions

from pottercon.api.models import ApiUser


class PotterPermission(permissions.BasePermission):
    """Return True if there is an authenticated user.

    The user in request is set in api.authentication.PotterAuthentication
    that only happens if the auth is good.
    """

    def has_permission(self, request, view):
        return isinstance(request.user, ApiUser)
