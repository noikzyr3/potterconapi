# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-12 11:19
from __future__ import unicode_literals

from django.db import migrations


def add_collectible_types(apps, schema_editor):
    collectible_type = apps.get_model('api', 'CollectibleType')
    criatura = collectible_type()
    criatura.name = 'Criatura'
    criatura.icon = 'collectible_type_default.jpg'
    criatura.save()
    ubicacion = collectible_type()
    ubicacion.name = 'Ubicación'
    ubicacion.icon = 'collectible_type_default.jpg'
    ubicacion.save()


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auto_20171111_2243'),
    ]

    operations = [
        migrations.RunPython(add_collectible_types),
    ]
