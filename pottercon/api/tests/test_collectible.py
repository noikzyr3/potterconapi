from django.urls import reverse

from rest_framework import status

from pottercon.api import factories
from . import base


class CollectibleTestCase(base.PotterTestCase):
    def setUp(self):
        self.user = factories.UserFactory()
        self.collectible = factories.CollectibleFactory()

    def test_list_collectibles__ok(self):
        url = reverse('collectible-list')
        self.prepare_login(user=self.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(response.data[0],
                         {'id': self.collectible.id,
                          'description': self.collectible.description,
                          'icon': self.collectible.icon,
                          'name': self.collectible.name,
                          'points': self.collectible.points,
                          'representation': self.collectible.representation,
                          'type': {
                              'icon': self.collectible.type.icon,
                              'id': self.collectible.type.id,
                              'name': self.collectible.type.name
                          }})

    def test_list_collectibles__401(self):
        url = reverse('collectible-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code,
                         status.HTTP_401_UNAUTHORIZED)
