from django import test

from pottercon.api import factories


class ModelsTestCase(test.TestCase):
    def setUp(self):
        self.user = factories.UserFactory()
        self.collectible1 = factories.CollectibleFactory()
        self.collectible2 = factories.CollectibleFactory(name='different')
        self.user.collectibles.add(self.collectible1)
        self.user.collectibles.add(self.collectible2)
        self.total_points = self.collectible1.points + self.collectible2.points

    def test_house_get_points(self):
        house = self.user.house
        self.assertEqual(house.points, self.total_points)

    def test_user_get_points(self):
        self.assertEqual(self.user.points, self.total_points)
