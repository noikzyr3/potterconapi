from django.urls import reverse

from rest_framework import status

from pottercon.api import factories
from pottercon.api import models

from . import base


class GetUserTests(base.PotterTestCase):
    def setUp(self):
        self.user = factories.UserFactory()
        self.collectible = factories.CollectibleFactory()
        self.user.collectibles.add(self.collectible)

    def test_user_basic_info(self):
        url = reverse('get-user', kwargs={'pk': 1})
        self.prepare_login(user=self.user)
        self.client.login()
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], self.user.id)
        self.assertEqual(response.data['device_id'], self.user.device_id)
        self.assertEqual(response.data['name'], self.user.name)
        self.assertEqual(response.data['image'], self.user.image)
        self.assertEqual(response.data['points'], self.user.points)
        self.assertEqual(response.data['house']['id'], self.user.house.id)
        self.assertEqual(response.data['house']['image'], self.user.house.image)
        self.assertEqual(response.data['house']['name'], self.user.house.name)
        self.assertEqual(response.data['house']['points'], self.user.house.points)

    def test_user_basic_info__no_auth_401(self):
        url = reverse('get-user', kwargs={'pk': 1})
        response = self.client.get(url)
        self.assertEqual(response.status_code,
                         status.HTTP_401_UNAUTHORIZED)


class CreateUserTests(base.PotterTestCase):
    def setUp(self):
        self.data = {
            'device_id': 'whatever',
            'name': 'Warry Putter',
            'image': 'whatever.jpg',
            'house': 1,
        }

    def test_new_user__good(self):
        url = reverse('create-user')
        self.assertEqual(models.ApiUser.objects.all().count(), 0)
        response = self.client.post(url, self.data, format='json')
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)
        self.assertEqual(response.data['id'], 1)
        self.assertEqual(models.ApiUser.objects.all().count(), 1)  # This guy

    def test_new_user__400(self):
        """Create user with not existing house id"""
        url = reverse('create-user')
        self.data['house'] = 123
        self.assertEqual(models.ApiUser.objects.all().count(), 0)
        response = self.client.post(url, self.data, format='json')
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)
        self.assertEqual(models.ApiUser.objects.all().count(), 0)


class GetUserCollectiblesTests(base.PotterTestCase):
    def setUp(self):
        self.user = factories.UserFactory()
        self.collectible = factories.CollectibleFactory()
        self.user.collectibles.add(self.collectible)

    def test_get_collectible_list(self):
        """Get the list of collectibles for self.user"""
        url = reverse('user-collectible-list', kwargs={'pk': self.user.id})
        self.prepare_login(user=self.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        expected = {
            'collectibles': [{
                'id': self.collectible.id,
                'name': self.collectible.name,
                'description': self.collectible.description,
                'icon': self.collectible.icon,
                'points': self.collectible.points,
                'representation': self.collectible.representation,
                'type': {
                    'id': self.collectible.type.id,
                    'name': self.collectible.type.name,
                    'icon': self.collectible.type.icon,
                }
            }]
        }
        self.assertEqual(response.data, expected)


class AchieveCollectibleTests(base.PotterTestCase):
    def setUp(self):
        self.collectible = factories.CollectibleFactory()
        self.user = factories.UserFactory()
        self.url = reverse('achieve-collectible')
        self.data = {'user_id': self.user.id,
                     'collectible_id': self.collectible.id}

    def test_achieve_collectible__good(self):
        self.prepare_login(self.user)
        response = self.client.post(self.url, self.data, format='json')
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)
        self.assertEqual(self.user.collectibles.first(),
                         self.collectible)

    def test_achieve_collectible__400(self):
        """Id doesn't exist"""
        self.data['collectible_id'] = 'whatever'
        self.prepare_login()
        response = self.client.post(self.url, self.data, format='json')
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)

    def test_achieve_collectible__401(self):
        """Wrong credentials"""
        response = self.client.post(self.url, self.data, format='json')
        self.assertEqual(response.status_code,
                         status.HTTP_401_UNAUTHORIZED)

    def test_achieve_collectible__409(self):
        """Collectible already achieved"""
        self.prepare_login()
        self.client.post(self.url, self.data, format='json')
        self.assertEqual(self.user.collectibles.count(), 1)
        response = self.client.post(self.url, self.data, format='json')
        self.assertEqual(response.status_code,
                         status.HTTP_409_CONFLICT)
