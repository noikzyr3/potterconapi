import base64

from rest_framework import test

from pottercon.api import factories


class PotterTestCase(test.APITestCase):
    def prepare_login(self, user=None):
        if not user:
            user = factories.UserFactory()
        credentials = '{}:{}'.format(user.id,
                                     user.device_id)
        auth = 'basic {}'.format(
            base64.b64encode(credentials.encode()).decode()
        )
        self.client.credentials(HTTP_AUTHORIZATION=auth)
