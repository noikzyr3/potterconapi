import datetime

from django.urls import reverse

from rest_framework import status

from pottercon.api import factories
from . import base


class EventTests(base.PotterTestCase):
    def setUp(self):
        self.event = factories.EventFactory()

    def test_list(self):
        self.prepare_login()
        url = reverse('event-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        expected = {'id': self.event.id,
                    'date': self.event.date,
                    'name': self.event.name,
                    'location': self.event.location,
                    'type': self.event.type.name,
                    'organizer': self.event.organizer,
                    'icon': self.event.icon}
        self.assertDictEqual(response.data[0], expected)

    def test_events_list_last_3(self):
        [factories.EventFactory(
            name='{}{}'.format('Evento', n),
            date=datetime.datetime.now() + datetime.timedelta(hours=1)
        ) for n in range(4)]
        url = reverse('next-events')
        response = self.client.get(url)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)
        for event in response.data:
            self.assertIn('Evento', event['name'])
