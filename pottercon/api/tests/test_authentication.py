import hashlib
import unittest

from pottercon.api import factories
from pottercon.api import authentication

from rest_framework import exceptions


class AuthenticationTestCase(unittest.TestCase):
    def setUp(self):
        # Factory is prepared with pass '123' in sha256
        self.user = factories.UserFactory()
        self.auth = authentication.PotterAuthentication()

    def test_non_existing_user(self):
        self.assertRaises(exceptions.AuthenticationFailed,
                          self.auth.authenticate_credentials,
                          999,
                          'whatever')

    def test_wrong_device_id(self):
        pwd = hashlib.sha256(b'whatever').hexdigest()
        self.assertRaises(exceptions.AuthenticationFailed,
                          self.auth.authenticate_credentials,
                          self.user.id,
                          pwd)

    def test_authentication(self):
        pwd = hashlib.sha256(b'123').hexdigest()
        self.assertTupleEqual(
            self.auth.authenticate_credentials(self.user.id,
                                               pwd),
            (self.user, None)
        )
