import datetime

from django.urls import reverse
from rest_framework import status

from pottercon.api import factories
from pottercon.api import models
from . import base


class DashboardTests(base.PotterTestCase):
    def setUp(self):
        creature_type = models.CollectibleType.objects.get(pk=1)
        location_type = models.CollectibleType.objects.get(pk=2)
        d = datetime.datetime.now() + datetime.timedelta(hours=1)
        self.events = [factories.EventFactory(date=d, name=str(n))
                       for n in range(3)]
        self.user = factories.UserFactory()
        creature1 = factories.CollectibleFactory(type=creature_type)
        creature2 = factories.CollectibleFactory(type=creature_type,
                                                 name='Ogro')
        location = factories.CollectibleFactory(type=location_type)
        self.user.collectibles.add(creature1, creature2, location)

    def test_dashboard_events__ok(self):
        url = reverse('dashboard-events')
        self.prepare_login()
        response = self.client.get(url)

        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        expected_keys = ['events', 'creatures_collected',
                         'locations_collected', 'houses']
        [self.assertIn(key, response.data.keys()) for key in expected_keys]
        self.assertEqual(len(response.data.keys()), len(expected_keys))
        self.assertEqual(response.data['creatures_collected'], 2)
        self.assertEqual(response.data['locations_collected'], 1)
        # [self.assertIn(event, response.data['events']) for event in self.events]
        # Houses is not tested because it is an hyperlink of a tested view
        # TODO(Noik): Test with date

    def test_dashboard_events__401(self):
        url = reverse('dashboard-events')
        response = self.client.get(url)
        self.assertEqual(response.status_code,
                         status.HTTP_401_UNAUTHORIZED)
