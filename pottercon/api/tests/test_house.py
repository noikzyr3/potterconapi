from django.urls import reverse

from rest_framework import status

from pottercon.api import factories
from pottercon.api import models
from . import base


class HouseTests(base.PotterTestCase):
    def setUp(self):
        self.house = models.House.objects.all()[0]
        self.collectible = factories.CollectibleFactory()

    def test_house_list(self):
        self.house.points = 10
        self.house.save()
        url = reverse('house-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        target_house = None
        for house in response.data:
            if house['name'] == self.house.name:
                target_house = house
                break
        self.assertIsNotNone(target_house)
        self.assertEqual(target_house,
                         {'id': self.house.id,
                          'name': self.house.name,
                          'image': self.house.image,
                          'points': 10})

    def test_house_sum_points(self):
        house = models.House.objects.all()[0]
        points = house.points
        url = reverse('house-points')
        response = self.client.post(url, data={'house_id': house.id,
                                               'points': 15})
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        updated_house = models.House.objects.get(pk=house.id)
        self.assertEqual(updated_house.points, points + 15)
