from rest_framework import exceptions
from rest_framework import serializers
from pottercon.api import models


class EventSerializer(serializers.ModelSerializer):
    type = serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='name',
    )
    date = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S')

    class Meta:
        model = models.Event
        fields = '__all__'


class HouseSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.House
        fields = ('id', 'name', 'image', 'points')


class CollectibleTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CollectibleType
        fields = '__all__'


class CollectibleSerializer(serializers.ModelSerializer):
    type = CollectibleTypeSerializer()

    class Meta:
        model = models.Collectible
        fields = ('id',
                  'name',
                  'description',
                  'icon',
                  'points',
                  'representation',
                  'type')


class UserSerializer(serializers.ModelSerializer):
    house = HouseSerializer(read_only=True)

    class Meta:
        model = models.ApiUser
        fields = ('id',
                  'device_id',
                  'name',
                  'image',
                  'house',
                  'points')


class CreateUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.ApiUser
        fields = '__all__'


class UserCollectiblesSerializer(serializers.ModelSerializer):
    collectibles = CollectibleSerializer(many=True)

    class Meta:
        model = models.ApiUser
        fields = ('collectibles',)


class AchieveCollectibleSerializer(serializers.Serializer):
    user_id = serializers.IntegerField()
    collectible_id = serializers.CharField()

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    def save(self, **kwargs):
        """Add collectible to user"""
        user = models.ApiUser.objects.get(
            pk=self.validated_data['user_id']
        )
        collectible = models.Collectible.objects.get(
            pk=self.validated_data['collectible_id']
        )
        if collectible in user.collectibles.all():
            raise exceptions.APIException(
                'Collectible already achieved'
            )
        user.collectibles.add(collectible)

    def validate(self, data):
        """Validate if user and collectible exists"""
        user_exist = models.ApiUser.objects.filter(
            pk=data['user_id']
        ).exists()
        collectible_exist = models.Collectible.objects.filter(
            pk=data['collectible_id']
        ).exists()

        if not user_exist:
            raise serializers.ValidationError('Wrong User')
        if not collectible_exist:
            raise serializers.ValidationError('Wrong Collectible')

        return data


class DashboardEventsSerializer(serializers.Serializer):
    events = serializers.ListField(
        child=EventSerializer(),
        write_only=True
    )
    creatures_collected = serializers.IntegerField()
    locations_collected = serializers.IntegerField()
    houses = serializers.ListField(
        child=HouseSerializer(),
        write_only=True
    )


class SumHousePointsSerializer(serializers.Serializer):
    house_id = serializers.IntegerField()
    points = serializers.IntegerField()

    def validate(self, data):
        if not models.House.objects.filter(pk=data['house_id']).exists():
            raise serializers.ValidationError('Wrong House')

        return data

    def save(self, **kwargs):
        house = models.House.objects.get(pk=self.validated_data['house_id'])
        house.points += self.validated_data['points']
        house.save()
