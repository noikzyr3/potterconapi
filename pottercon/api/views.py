from django.utils import timezone

from rest_framework import exceptions
from rest_framework import generics
from rest_framework import response
from rest_framework import status
from rest_framework import views

from pottercon.api import models
from pottercon.api import permissions
from pottercon.api import serializers


class HouseList(generics.ListAPIView):
    """List of Houses"""
    queryset = models.House.objects.all()
    serializer_class = serializers.HouseSerializer
    permission_classes = tuple()


class CollectibleList(generics.ListAPIView):
    """List of Collectibles"""
    queryset = models.Collectible.objects.all()
    serializer_class = serializers.CollectibleSerializer
    permission_classes = tuple()


class EventList(generics.ListAPIView):
    """List of Events"""
    queryset = models.Event.objects.all()
    serializer_class = serializers.EventSerializer
    permission_classes = tuple()


class NextEventList(generics.ListAPIView):
    """List of next 3 Events"""
    queryset = models.Event.objects.filter(date__gt=timezone.now())[:3]
    serializer_class = serializers.EventSerializer
    permission_classes = tuple()


class UserRetrieve(generics.RetrieveAPIView):
    """Get User info"""
    queryset = models.ApiUser.objects.all()
    serializer_class = serializers.UserSerializer
    permission_classes = (permissions.PotterPermission,)


class UserCreate(generics.CreateAPIView):
    """Create User"""
    queryset = models.ApiUser.objects.all()
    serializer_class = serializers.CreateUserSerializer
    permission_classes = (permissions.PotterPermission,)


class UserCollectibles(generics.RetrieveAPIView):
    """Get collectible list from user"""
    queryset = models.ApiUser.objects.all()
    serializer_class = serializers.UserCollectiblesSerializer
    permissions_classes = (permissions.PotterPermission,)


class AchieveCollectible(views.APIView):
    """User Achieve collectible"""
    permission_classes = (permissions.PotterPermission,)

    def post(self, request):
        serializer = serializers.AchieveCollectibleSerializer(
            data=request.data
        )
        if serializer.is_valid():
            try:
                serializer.save()
            except exceptions.APIException:
                return response.Response(
                    serializer.errors, status=status.HTTP_409_CONFLICT
                )
        else:
            return response.Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )
        return response.Response(
            serializer.data, status=status.HTTP_201_CREATED
        )


class DashboardEvents(views.APIView):
    """List of the next 3 events, with total collectibles"""

    def get(self, request):
        # Collectibles
        creatures_collected = request.user.collectibles.filter(type_id=1).count()
        locations_collected = request.user.collectibles.filter(type_id=2).count()

        # Events
        events = models.Event.objects.filter(date__gt=timezone.now())[:3]

        # Houses
        houses = models.House.objects.all()

        dashboard_serializer = serializers.DashboardEventsSerializer(
            data={'events': [event.__dict__ for event in events],
                  'creatures_collected': creatures_collected,
                  'locations_collected': locations_collected,
                  'houses': [house.__dict__ for house in houses]}
        )

        if dashboard_serializer.is_valid():
            return response.Response(
                dashboard_serializer.validated_data,
                status=status.HTTP_200_OK,
            )
        else:
            return response.Response(
                dashboard_serializer.errors,
                status=status.HTTP_400_BAD_REQUEST,
            )


class HousePoints(views.APIView):
    """Sum points to House"""

    def post(self, request):
        serializer = serializers.SumHousePointsSerializer(
            data=request.data
        )
        if serializer.is_valid():
            serializer.save()
            return response.Response(
                serializer.data, status=status.HTTP_200_OK
            )
        else:
            return response.Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST,
            )

