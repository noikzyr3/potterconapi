from django.contrib import admin

from . import models


admin.site.register(models.EventType)
admin.site.register(models.Event)
admin.site.register(models.House)
admin.site.register(models.CollectibleType)
admin.site.register(models.Collectible)
