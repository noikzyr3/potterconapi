from django.apps import AppConfig


class ApiConfig(AppConfig):
    name = 'pottercon.api'
