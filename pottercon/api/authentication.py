from django.utils.translation import ugettext_lazy as _

from rest_framework import authentication
from rest_framework import exceptions

from pottercon.api import models


class PotterAuthentication(authentication.BasicAuthentication):
    def authenticate_credentials(self, userid, deviceid, request=None):
        """Authenticate the user_id and Device_id"""

        try:
            user = models.ApiUser.objects.get(pk=userid)
        except models.ApiUser.DoesNotExist:
            raise exceptions.AuthenticationFailed(
                _('Invalid user id.')
            )

        if deviceid != user.device_id:
            raise exceptions.AuthenticationFailed(
                _('Invalid device id.')
            )

        request.user = user
        return user, None
