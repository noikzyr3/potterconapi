from django.db import models


class EventType(models.Model):
    name = models.CharField(max_length=100)
    icon = models.CharField(max_length=250)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


class Event(models.Model):
    date = models.DateTimeField()
    name = models.CharField(max_length=150, default='Evento sin nombre')
    location = models.CharField(max_length=150)
    type = models.ForeignKey(EventType, related_name='type')
    organizer = models.CharField(max_length=100, null=True)
    icon = models.CharField(max_length=250, null=True)

    class Meta:
        ordering = ['date']

    def __str__(self):
        return self.name


class House(models.Model):
    name = models.CharField(max_length=50)
    image = models.CharField(max_length=250)
    points = models.BigIntegerField(default=0)

    # @property
    # def points(self):
    #     return sum([user.points for user in self.user.all()])

    def __str__(self):
        return self.name


class CollectibleType(models.Model):
    """Creature and Location"""
    name = models.CharField(max_length=100)
    icon = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Collectible(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    name = models.CharField(max_length=100)
    description = models.TextField(null=True)
    points = models.IntegerField(default=1)
    type = models.ForeignKey(CollectibleType)
    icon = models.CharField(max_length=100, null=True)
    representation = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.name


class ApiUser(models.Model):
    device_id = models.CharField(max_length=250, unique=True, null=True)
    name = models.CharField(max_length=50)
    image = models.CharField(max_length=250)
    house = models.ForeignKey(House, related_name='user')
    collectibles = models.ManyToManyField(Collectible, null=True)
    # TODO(NoiK): device_id should be hashed with sha256 when saved

    @property
    def points(self):
        """Get the points for this user"""
        return sum([c.points for c in self.collectibles.all()])
