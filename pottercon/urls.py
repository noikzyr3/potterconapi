from django.conf.urls import url
from django.contrib import admin

from rest_framework.urlpatterns import format_suffix_patterns

from pottercon.api import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),  # TODO(noik): Remove when data set
    url(r'api/house/?$',
        views.HouseList.as_view(),
        name='house-list'),
    url(r'api/collectible/?$',
        views.CollectibleList.as_view(),
        name='collectible-list'),
    url(r'api/event/?$',
        views.EventList.as_view(),
        name='event-list'),
    url(r'api/user/(?P<pk>\d+)/?$',
        views.UserRetrieve.as_view(),
        name='get-user'),
    url(r'api/user/?$',
        views.UserCreate.as_view(),
        name='create-user'),
    url(r'api/user/(?P<pk>\d+)/collectible/?$',
        views.UserCollectibles.as_view(),
        name='user-collectible-list'),
    url(r'api/user/collectible/?$',
        views.AchieveCollectible.as_view(),
        name='achieve-collectible'),
    url(r'api/dashboard/?$',
        views.DashboardEvents.as_view(),
        name='dashboard-events'),
    url(r'api/house/points/?$',
        views.HousePoints.as_view(),
        name='house-points'),
    url(r'api/event/next/?$',
        views.NextEventList.as_view(),
        name='next-events')
]

urlpatterns = format_suffix_patterns(urlpatterns)
